import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-docbook',
  templateUrl: 'docbook.html',
})
export class DocbookPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DocbookPage');
  }

  open_page(page){
    this.navCtrl.push(page);
  }

  goBack(){
    this.navCtrl.pop();
  }

}
