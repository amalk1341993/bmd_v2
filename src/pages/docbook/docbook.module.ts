import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DocbookPage } from './docbook';

@NgModule({
  declarations: [
    DocbookPage,
  ],
  imports: [
    IonicPageModule.forChild(DocbookPage),
  ],
})
export class DocbookPageModule {}
