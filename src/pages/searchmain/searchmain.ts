import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-searchmain',
  templateUrl: 'searchmain.html',
})
export class SearchmainPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchmainPage');
  }

  goBack(){
    this.navCtrl.pop();
  }

  open_page(page){
    this.navCtrl.push(page);
  }

}
