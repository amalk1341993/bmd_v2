import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchmainPage } from './searchmain';

@NgModule({
  declarations: [
    SearchmainPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchmainPage),
  ],
})
export class SearchmainPageModule {}
