import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-speciality',
  templateUrl: 'speciality.html',
})
export class SpecialityPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpecialityPage');
  }

  dismiss() {
  this.viewCtrl.dismiss();
}

}
