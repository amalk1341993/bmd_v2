import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpecialityPage } from './speciality';

@NgModule({
  declarations: [
    SpecialityPage,
  ],
  imports: [
    IonicPageModule.forChild(SpecialityPage),
  ],
})
export class SpecialityPageModule {}
