import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BmdPopoverPage } from './bmd-popover';

@NgModule({
  declarations: [
    BmdPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(BmdPopoverPage),
  ],
})
export class BmdPopoverPageModule {}
