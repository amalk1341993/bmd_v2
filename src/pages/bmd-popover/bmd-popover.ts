import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-bmd-popover',
  templateUrl: 'bmd-popover.html',
})
export class BmdPopoverPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BmdPopoverPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
