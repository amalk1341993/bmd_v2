import { Component } from '@angular/core';
import { IonicPage, NavController,PopoverController,ModalController,NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public popoverCtrl: PopoverController,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  presentPopover(myEvent) {
   let popover = this.popoverCtrl.create('BmdPopoverPage');
   popover.present({
     ev: myEvent
   });
 }

 specialitymodal() {
   let profileModal = this.modalCtrl.create('SpecialityPage');
   profileModal.present();
 }

 open_page(page){
   this.navCtrl.push(page);
 }

}
